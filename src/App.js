import './App.css';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import Gallery from './pages/Gallery';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import ProductView from './pages/ProductView';
import AdminDashboard from './pages/AdminDashboard'
import GalleryDashboard from './pages/GalleryDashboard'
import Cart from './pages/Order'

import { UserProvider } from './UserContext';
import { useState, useEffect, useContext } from 'react'; 

function App() {

  const [user, setUser] = useState({
    id: localStorage.getItem('id')
  })

  const unsetUser = () => {
    localStorage.clear()
  }


  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
    <Router>
      <Container fluid>
        <AppNavbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/admin" element={<AdminDashboard />} />
          <Route path="/photos" element={<GalleryDashboard />} />
          <Route path="/products" element={<Products />} />
          <Route path="/gallery" element={<Gallery />} />
          <Route path="/products/:_id" element={<ProductView />} />
          <Route path="/orders/:_id" element={<Cart />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="*" element={<Error />} />

        < /Routes>
      </Container>
    < /Router>
    < /UserProvider>
  );
}

export default App;

