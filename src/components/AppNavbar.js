
import {Container, Navbar, Nav, Image} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom'
import {useState, Fragment, useContext} from 'react'
import UserContext from '../UserContext'
import logo from './logo.png'
export default function AppNavbar(){
	// const [user, setUser] = useState(localStorage.getItem('email'));
	//const { user } = useContext(UserContext)
	const [userId, setUserId] = useState(localStorage.getItem("userId"));
	const [userRole, setUserRole] = useState(localStorage.getItem("userRole"));
	
	console.log("result of userId from localStorage: ")
	console.log(userId);
	console.log("result of userRole localStorage: ")
	console.log(userRole);
	
	return(
		<Container fluid>
		<Navbar bg="light" expand="md">
				        <Navbar.Brand as={Link} to="/">
				        	<img
				        	             alt="logo"
				        	             src={logo}
				        	             width="30"
				        	             height="30"
				        	             className="d-flex"
				        	           />{' '}
				        </Navbar.Brand>
				        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
				        <Navbar.Collapse id="basic-navbar-nav">
				            <Nav className="ml-auto">
					            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
					            <Nav.Link as={NavLink} to="/gallery">Gallery</Nav.Link>
					            <Nav.Link as={NavLink} to="/story">Our Story</Nav.Link>
					            <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
					            {(userId !== null)?
					            	<Fragment>
						            	{(userRole)
						            	?
						            	<>
						            		<Nav.Link as={NavLink} to="/admin">Admin Dashboard</Nav.Link>
						            		<Nav.Link as={NavLink} to="/photos">Gallery Dashboard</Nav.Link>
						            		<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
						            	</>
						            	:
						            	<Fragment>
						            	    <Nav.Link as={NavLink} to="/orders">Cart</Nav.Link>
						            		<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
						            	    </Fragment>
						            	            	
						            	}
					            	</Fragment>
					            	:
					            	<Fragment>
						            	<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
						            	<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
					            	</Fragment>
					        	}

				            </Nav>
				        </Navbar.Collapse>
					</Navbar>
				</Container>
	)
}



