import { Button, Carousel, Col, Container} from 'react-bootstrap';
import { useState, useEffect, useContext} from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom"

export default function GalleryCarousel({galleryProp}){


	const {_id, name, kind, image} = galleryProp;
	
	return(

		<Container className="p-3" style={{ height: 480, width: 250}} >
			<h5 className="text-center">{kind}</h5>
		      <img className="d-block w-100 p-1 mt-2 mb-2 border rounded bg-light" src={image} style={{ height: 300, width: 220}} alt="{name}" />
		      
		      <p className="text-center pt-1"><i>{name}</i></p>
		</Container>		        
		    	  
		  
	);
}