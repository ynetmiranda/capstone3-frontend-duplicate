import { Button, Card, Row, Container, Col} from 'react-bootstrap';
import { useState, useEffect, useContext} from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom"

export default function ProductCard({productProp}){

	const {_id, name, productImage, description, price, orderSlotsAvailable} = productProp;
	
	return(
		    <Card as={Col} className="p-3 m-3" style={{ width: '18rem' }}>
		      <Card.Img variant="top" src={productImage} style={{ height: '20rem'}}/>
		      <Card.Body>
		        <Card.Title>{name}</Card.Title>
		        <Card.Text>
		          <i>{description}</i>
		        </Card.Text>
		        <Card.Text>
		          Price: starts at {price} <br/>
		          Order Slots: {orderSlotsAvailable}
		        </Card.Text>
		        <Button as={Link} to={`/products/${_id}`}>Order</Button>
		      </Card.Body>
		    </Card>
		    	  
		  
	);
}