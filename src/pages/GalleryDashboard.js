import {useContext, useEffect, useState} from "react";
import FileBase64 from 'react-file-base64';

import { Button, Table, Modal, Form } from "react-bootstrap";
import {Navigate} from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function GalleryDashboard(){


	const [userRole, setUserRole] = useState(localStorage.getItem("userRole"));

	
	const [allPhotos, setAllPhotos] = useState([]);

	const [_id, set_id] = useState("");
	const [name, setName] = useState("");
	const [kind, setKind] = useState("");
	const [image, setImage] = useState("");

    const [isActive, setIsActive] = useState(false);

   
    const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);

	
	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)

	
	const openEdit = (_id) => {
		set_id(_id);

		
		fetch(`${ process.env.REACT_APP_API_URL }/photos/${_id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			
			setName(data.name);
			setKind(data.kind);
			setImage(data.image);
		});

		setShowEdit(true)
	};

	const closeEdit = () => {

		
	    setName('');
	    setKind('');
	    setImage('');

		setShowEdit(false);
	};


	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/photos/allPhotos`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllPhotos(data.map(photo => {
				return (
					<tr key={photo._id}>
						<td>{photo._id}</td>
						<td>{photo.name}</td>
						<td>{photo.kind}</td>
						<td>{photo.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								
								(photo.isActive)
								?
									<Button variant="danger" size="sm" onClick={() => archive(photo._id, photo.name)}>Archive</Button>
								:
									<>
										<Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(photo._id, photo.name)}>Unarchive</Button>
										<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(photo._id)}>Edit</Button>
									</>

							}
						</td>
					</tr>
				)
			}));
		});
	}


	useEffect(()=>{
		fetchData();
	}, [])


	const archive = (_id, photoName) =>{
		console.log(_id);
		console.log(photoName);

		
		fetch(`${process.env.REACT_APP_API_URL}/photos/${_id}/archivePhoto`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${photoName} is now inactive.`
				});
		
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}


	const unarchive = (_id, photoName) =>{
		console.log(_id);
		console.log(photoName);

		
		fetch(`${process.env.REACT_APP_API_URL}/photos//${_id}/archivePhoto`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${photoName} is now active.`
				});
		
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}


	const addPhoto = (e) =>{
			
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/photos/addPhoto`, {
		    	method: "POST",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    kind: kind,
				    image: image
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Photo succesfully added",
		    		    icon: "success",
		    		    text: `${name} is now added`
		    		});

		    	
		    		fetchData();
		    		
		    		closeAdd();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeAdd();
		    	}

		    })

		    
		    setName('');
		    setKind('');
		    setImage('');
	}


	const updatePhoto = (e) =>{
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/photos/${_id}/updatePhoto`, {
		    	method: "PUT",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    kind: kind,
				    image: image
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Photo succesfully updated",
		    		    icon: "success",
		    		    text: `${name} is now updated`
		    		});

		    		fetchData();
	
		    		closeEdit();

		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});

		    		closeEdit();
		    	}

		    })

		
		    setName('');
		    setKind('');
		    setImage('');
	} 

	
	useEffect(() => {

       
        if(name !== "" && kind !== "" && image !== ""){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, kind, image]);

	return(
		(userRole)
		?
		<>
			{/*Header for the admin dashboard and functionality for create course and show enrollments*/}
			<div className="mt-5 mb-3 text-center">
				<h1>Gallery Dashboard</h1>
				{/*Adding a new course */}
				<Button variant="success" className="
				mx-2" onClick={openAdd}>Add Photo</Button>
				{/*To view all the user enrollments*/}
				<Button variant="secondary" className="
				mx-2">Show Photos</Button>
			</div>
			{/*End of admin dashboard header*/}

			{/*For view all the courses in the database.*/}
			<Table striped bordered hover>
		      <thead>
		        <tr>
		          <th>Photo ID</th>
		          <th>Name</th>
		          <th>Type of Cake</th>
		          <th>Status</th>
		          <th>Actions</th>
		        </tr>
		      </thead>
		      <tbody>
	        	{allPhotos}
		      </tbody>
		    </Table>
			{/*End of table for course viewing*/}

	    	{/*Modal for Adding a new course*/}
	        <Modal show={showAdd} fullscreen={true} onHide={closeAdd}>
	    		<Form onSubmit={e => addPhoto(e)}>

	    			<Modal.Header closeButton>
	    				<Modal.Title>Add New Photo</Modal.Title>
	    			</Modal.Header>

	    			<Modal.Body>
	    	        	<Form.Group controlId="name" className="mb-3">
	    	                <Form.Label>Photo Name</Form.Label>
	    	                <Form.Control 
	    		                type="text" 
	    		                placeholder="Enter Photo Name" 
	    		                value = {name}
	    		                onChange={e => setName(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	             <Form.Group controlId="kind" className="mb-3">
	    	                <Form.Label>Kind</Form.Label>
	    	                <Form.Control
	    	                	as="textarea"
	    	                	rows={3}
	    		                placeholder="Enter type of cake" 
	    		                value = {kind}
	    		                onChange={e => setKind(e.target.value)}
	    		                required
	    	                />
	    	            </Form.Group>

	    	                 <Form.Group controlId="name" className="mb-3">
	    	                <Form.Label>Image</Form.Label>
	    	            <div className="container">
	    	                <pre>{JSON.stringify(image, null, '\t')}</pre>
	    	                
	    	                  <FileBase64
	    	                    type="file"
	    	                    multiple={false}
	    	                    onDone={({ base64 }) => setImage(base64)}
	    	                  />
	    	                  <div className="right-align">
	    	                  </div>

	    	               
	    	              </div>
	    	            </Form.Group>
	    			</Modal.Body>

	    			<Modal.Footer>
	    				{ isActive 
	    					? 
	    					<Button variant="primary" type="submit" id="submitBtn">
	    						Save
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Save
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closeAdd}>
	    					Close
	    				</Button>
	    			</Modal.Footer>

	    		</Form>	
	    	</Modal>
	    {/*End of modal for adding course*/}

    	{/*Modal for Editing a course*/}
        <Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
    		<Form onSubmit={e => updatePhoto(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title>Edit a Photo</Modal.Title>
    			</Modal.Header>

    			<Modal.Body>
    	        	<Form.Group controlId="name" className="mb-3">
    	                <Form.Label>Photo Name</Form.Label>
    	                <Form.Control 
    		                type="text" 
    		                placeholder="Enter Photo Name" 
    		                value = {name}
    		                onChange={e => setName(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	            <Form.Group controlId="kind" className="mb-3">
    	                <Form.Label>Kind</Form.Label>
    	                <Form.Control
    	                	as="textarea"
    	                	rows={3}
    		                placeholder="Enter type of cake" 
    		                value = {kind}
    		                onChange={e => setKind(e.target.value)}
    		                required
    	                />
    	            </Form.Group>

    	             <Form.Group controlId="name" className="mb-3">
    	                <Form.Label>Image</Form.Label>
    	            	
    	                <div className="container">
    	                    <pre>{JSON.stringify(image, null, '\t')}</pre>
    	                    
    	                      <FileBase64
    	                        type="file"
    	                        multiple={false}
    	                        onDone={({ base64 }) => setImage(base64)}
    	                      />
    	                      <div className="right-align">
    	                      </div>

    	                   
    	                  </div>

    	            </Form.Group>

    	          
    			</Modal.Body>

    			<Modal.Footer>
    				{ isActive 
    					? 
    					<Button variant="primary" type="submit" id="submitBtn">
    						Save
    					</Button>
    				    : 
    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
    				    	Save
    				    </Button>
    				}
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>

    		</Form>	
    	</Modal>
 
		</>
		:
		<Navigate to="/gallery" />
	)
}
