import { Fragment} from 'react';
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import logo from '../components/logo.png'
import {Container, Row, Col} from 'react-bootstrap'

const data = {
	title: "Our Story",
	destination: "/"
}
export default function Home() {
	return (
		<Fragment>
		<Container className="justify-content-center">
			<Row xs={1} md={4} lg={8}>
				<Col >
			    <img alt="logo" src={logo} width="500" height="500" className="d-inline-block align-top"/>
			    </Col>		  
		    </Row>
		    <Banner data={data} className="justify-content-center" />
		    <Highlights />
		    
		</Container>
		</Fragment>

	)
}