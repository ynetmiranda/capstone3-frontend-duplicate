import { useState, useEffect, useContext } from "react";

import {Container, Card, Button, Row, Col} from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function ProductView(){

	const { user } = useContext(UserContext);

	const  {_id}  = useParams();

	const navigate = useNavigate();


	const [name, setName] = useState('');
	const [productImage, setProductImage] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [orderSlotsAvailable, setOrderSlotsAvailable] = useState(0);

	useEffect(()=>{
		console.log(_id);

		fetch(`${ process.env.REACT_APP_API_URL }/products/${_id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setProductImage(data.productImage);
			setDescription(data.description);
			setPrice(data.price);
			setOrderSlotsAvailable(data.orderSlotsAvailable)

		});

	}, [_id])

	const addItemToCart = (_id) =>{

		fetch(`${ process.env.REACT_APP_API_URL }/orders/addToCart`, {
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				_id: _id
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log("Order data: ")
			console.log(data);

			if(data){
				Swal.fire({
					title: "Successfully added",
					icon: "success",
					text: "You have successfully added this product."
				});

				navigate("/orders/6423ed71b305151c8c582362");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}

		});

	}

	return(
		<Container fluid className="mt-5">
					<Row className="justify-content-center">
						<Col  xs={10} md={8} lg={4}>
							<Card>
								<Card.Body className="text-center">
									<Card.Title>{name}</Card.Title>
									<Card.Img variant="top" src={productImage} className="mb-3" />
									<Card.Subtitle>Description:</Card.Subtitle>
									<Card.Text>{description}</Card.Text>
									<Card.Subtitle>Price:</Card.Subtitle>
									<Card.Text>PhP {price}</Card.Text>
									<Card.Subtitle>Order Slots</Card.Subtitle>
									<Card.Text>{orderSlotsAvailable}</Card.Text>
									{
										(user.id !== null)
										?
											<Button variant="primary"  size="lg" onClick={() => addItemToCart(_id)}>Order</Button>
										:
											<Button as={Link} to="/login" variant="success"  size="lg">Login to Order</Button>
									}
								</Card.Body>		
							</Card>
						</Col>
					</Row>
				</Container>

	)
}




